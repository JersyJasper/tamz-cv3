package com.example.savingscalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class Settings extends AppCompatActivity {

    final static int PIECHART =  1;
    final static int BARCHART =  2;

    PieChart pieChart;
    BarChart barChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        pieChart = findViewById(R.id.pieChart1);
        barChart = findViewById(R.id.barChart1);
        Intent intent = getIntent();
        int chart_type = intent.getIntExtra("chart_type" , 1);
        if (chart_type == PIECHART)
        {
            pieChart.setAlpha(0.4f);
        }
        else if (chart_type == BARCHART)
        {
            barChart.setAlpha(0.4f);
        }
        initPieChart();
        showBarChart(50.0, 50.0);

    }

    public void click_PieChart(View view) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("result", PIECHART);

        setResult(RESULT_OK, resultIntent);
        finish();
    }

    public void click_BarChart(View view) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("result", BARCHART);

        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private void initPieChart(){
        //using percentage as values instead of amount
        pieChart.setUsePercentValues(true);

        //remove the description label on the lower left corner, default true if not set
        pieChart.getDescription().setEnabled(false);

        pieChart.setTouchEnabled(false);
        //setting the first entry start from right hand side, default starting from top
        pieChart.setRotationAngle(90);


        //setting the color of the hole in the middle, default white
        pieChart.setHoleColor(Color.parseColor("#000000"));

        showPieChart(50.0, 50.0);

    }


    public void showPieChart(double deposit_number, double result_rate_saved)
    {
        ArrayList<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry((float)deposit_number, "Vklad"));
        entries.add(new PieEntry((float)result_rate_saved, "Úroky"));

        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setTextSize(16f);
        l.setTextColor(Color.WHITE);
        l.setFormSize(16f);

        PieDataSet ds1 = new PieDataSet(entries, "");
        ds1.setValueTextSize(16f);
        ds1.setColors(ColorTemplate.MATERIAL_COLORS);
        ds1.setSliceSpace(2f);
        ds1.setValueTextColor(Color.WHITE);

        PieData pieData = new PieData(ds1);
        pieData.setValueFormatter(new PercentFormatter(pieChart));
        pieData.setDrawValues(true);

        pieChart.setData(pieData);
        pieChart.invalidate();
    }

    public void showBarChart(double deposit_number, double result_rate_saved) {
        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(0, (float)deposit_number));
        entries.add(new BarEntry(1, (float)result_rate_saved));

        Legend l = barChart.getLegend();
        l.setEnabled(false);

        BarDataSet barDataSet = new BarDataSet(entries, "");
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);

        String[] labels = new String[] {"Vklad", "Úroky"};
        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setTextColor(Color.WHITE);

        YAxis leftAxis = barChart.getAxisLeft();
        //hiding the left y-axis line, default true if not set
        leftAxis.setDrawAxisLine(false);
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setGridColor(Color.WHITE);

        YAxis rightAxis = barChart.getAxisRight();
        //hiding the right y-axis line, default true if not set
        rightAxis.setDrawAxisLine(false);
        rightAxis.setTextColor(Color.WHITE);
        rightAxis.setGridColor(Color.WHITE);

        BarData barData = new BarData(barDataSet);
        barChart.getDescription().setText("");

        barChart.setData(barData);
        barChart.invalidate();
    }

}