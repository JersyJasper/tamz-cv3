package com.example.savingscalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class History extends AppCompatActivity {

    ArrayList<String> arraylist =new ArrayList<String>();

    ListView list;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        list = findViewById(R.id.list);
        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arraylist);
        list.setAdapter(adapter);

        // Fetch all saved values
        SharedPreferences mPrefs = getSharedPreferences("IDvalue",0);
        Set<String> default_value = new HashSet<String>();
        Set<String> value = (mPrefs.getStringSet("History", default_value));
        for (String s : value)
        {
            adapter.add(s);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.history_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.clear_history:
                SharedPreferences mPrefs = getSharedPreferences("IDvalue",0);
                mPrefs.edit().remove("History").commit();
                adapter.clear();
                adapter.notifyDataSetChanged();
                return true;
        }
        return false;
    }
}