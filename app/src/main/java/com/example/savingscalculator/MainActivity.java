package com.example.savingscalculator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    EditText deposit;
    EditText rate;
    EditText time;
    TextView amount_saved;
    TextView rate_saved;
    PieChart pieChart;
    BarChart barChart;
    
    int chart_type = PIECHART;

    double result_rate_saved = 50;
    double deposit_number = 50;


    final static int PIECHART =  1;
    final static int BARCHART =  2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        deposit = findViewById(R.id.deposit_editText);
        rate = findViewById(R.id.rate_editText);
        time = findViewById(R.id.time_editText);
        amount_saved = findViewById(R.id.amount_saved_textView);
        rate_saved = findViewById(R.id.rate_saved_textView);
        pieChart = findViewById(R.id.pieChart1);
        barChart = findViewById(R.id.barChart1);
        barChart.setVisibility(View.GONE);
        initPieChart();
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent settings_intent = new Intent(this, Settings.class);
                settings_intent.putExtra("chart_type", chart_type);
                startActivityForResult(settings_intent, 123);
                return true;
            case R.id.history:
                Intent history_intent = new Intent(this, History.class);
                startActivity(history_intent);
                return true;
            case R.id.aboutapp:
                Intent aboutapp_intent = new Intent(this, Aboutapp.class);
                startActivity(aboutapp_intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 123) {
            chart_type = data.getIntExtra("result", 0);
            if (chart_type == PIECHART)
            {
                barChart.setVisibility(View.GONE);
                showPieChart(deposit_number, result_rate_saved);
            }
            else if (chart_type == BARCHART)
            {
                pieChart.setVisibility(View.GONE);
                showBarChart(deposit_number, result_rate_saved);
            }
        }

    }

    public void calculate_onClick(View view) {
        if (deposit.getText().length() == 0 || rate.getText().length() == 0 || time.getText().length() == 0.) {
            return;
        }

        deposit_number = Double.parseDouble(deposit.getText().toString());
        double rate_number = Double.parseDouble(rate.getText().toString());
        double time_number = Double.parseDouble(time.getText().toString());
        double result_amount_saved = deposit_number * (Math.pow(1+(rate_number/100),time_number));
        String amount_saved_text = "Naspořená suma: " + String.format("%.2f", result_amount_saved);
        amount_saved.setText(amount_saved_text);
        result_rate_saved = result_amount_saved - deposit_number;
        String rate_saved_text = "Z toho úroky: " + String.format("%.2f", result_rate_saved);
        rate_saved.setText(rate_saved_text);

        SharedPreferences mPrefs = getSharedPreferences("IDvalue", 0);
        Set<String> value= new HashSet<String>(mPrefs.getStringSet("History", new HashSet<String>()));
        String val = amount_saved_text + " | " + rate_saved_text;
        value.add(val);

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.remove("History");
        editor.putStringSet("History", value);
        editor.apply();

        if (chart_type == PIECHART)
        {
            barChart.setVisibility(View.GONE);
            showPieChart(deposit_number, result_rate_saved);
        }
        else if (chart_type == BARCHART)
        {
            pieChart.setVisibility(View.GONE);
            showBarChart(deposit_number, result_rate_saved);
        }

    }

    private void initBarChart(){
        //hiding the grey background of the chart, default false if not set
        barChart.setDrawGridBackground(false);
        //remove the bar shadow, default false if not set
        barChart.setDrawBarShadow(false);
        //remove border of the chart, default false if not set
        barChart.setDrawBorders(false);

        //remove the description label text located at the lower right corner
        Description description = new Description();
        description.setEnabled(false);
        barChart.setDescription(description);

        //setting animation for y-axis, the bar will pop up from 0 to its value within the time we set
        barChart.animateY(1000);
        //setting animation for x-axis, the bar will pop up separately within the time we set
        barChart.animateX(1000);

        XAxis xAxis = barChart.getXAxis();
        //change the position of x-axis to the bottom
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        //set the horizontal distance of the grid line
        xAxis.setGranularity(1f);
        //hiding the x-axis line, default true if not set
        xAxis.setDrawAxisLine(false);
        //hiding the vertical grid lines, default true if not set
        xAxis.setDrawGridLines(false);

        YAxis leftAxis = barChart.getAxisLeft();
        //hiding the left y-axis line, default true if not set
        leftAxis.setDrawAxisLine(false);
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setGridColor(Color.WHITE);

        YAxis rightAxis = barChart.getAxisRight();
        //hiding the right y-axis line, default true if not set
        rightAxis.setDrawAxisLine(false);
        rightAxis.setTextColor(Color.WHITE);
        rightAxis.setGridColor(Color.WHITE);

    }


    private void initPieChart(){
        //using percentage as values instead of amount
        pieChart.setUsePercentValues(true);

        //remove the description label on the lower left corner, default true if not set
        pieChart.getDescription().setEnabled(false);

        //enabling the user to rotate the chart, default true
        pieChart.setRotationEnabled(true);
        //adding friction when rotating the pie chart
        pieChart.setDragDecelerationFrictionCoef(0.9f);
        //setting the first entry start from right hand side, default starting from top
        pieChart.setRotationAngle(90);

        //highlight the entry when it is tapped, default true if not set
        pieChart.setHighlightPerTapEnabled(true);
        //setting the color of the hole in the middle, default white
        pieChart.setHoleColor(Color.parseColor("#000000"));

        showPieChart(50.0, 50.0);

    }


    public void showPieChart(double deposit_number, double result_rate_saved)
    {
        ArrayList<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry((float)deposit_number, "Vklad"));
        entries.add(new PieEntry((float)result_rate_saved, "Úroky"));

        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setTextSize(16f);
        l.setTextColor(Color.WHITE);
        l.setFormSize(16f);

        PieDataSet ds1 = new PieDataSet(entries, "");
        ds1.setValueTextSize(16f);
        ds1.setColors(ColorTemplate.MATERIAL_COLORS);
        ds1.setSliceSpace(2f);
        ds1.setValueTextColor(Color.WHITE);

        PieData pieData = new PieData(ds1);
        pieData.setValueFormatter(new PercentFormatter(pieChart));
        pieData.setDrawValues(true);

        pieChart.setData(pieData);
        pieChart.setVisibility(View.VISIBLE);
        pieChart.invalidate();
    }

    public void showBarChart(double deposit_number, double result_rate_saved) {
        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(0, (float)deposit_number));
        entries.add(new BarEntry(1, (float)result_rate_saved));


        Legend l = barChart.getLegend();
        l.setEnabled(false);

        BarDataSet barDataSet = new BarDataSet(entries, "");
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);

        String[] labels = new String[] {"Vklad", "Úroky"};
        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setTextColor(Color.WHITE);

        YAxis leftAxis = barChart.getAxisLeft();
        //hiding the left y-axis line, default true if not set
        leftAxis.setDrawAxisLine(false);
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setGridColor(Color.WHITE);

        YAxis rightAxis = barChart.getAxisRight();
        //hiding the right y-axis line, default true if not set
        rightAxis.setDrawAxisLine(false);
        rightAxis.setTextColor(Color.WHITE);
        rightAxis.setGridColor(Color.WHITE);


        BarData barData = new BarData(barDataSet);
        barChart.getDescription().setText("");

        barChart.setData(barData);
        barChart.setVisibility(View.VISIBLE);
        barChart.invalidate();

    }


}
